package Situacion2;

public class MateriaPrima extends Material {

    public MateriaPrima(int codigo, float precio, String nombre) {
        super(codigo, precio, nombre);
    }

    public void mostrarMaterial() {
        System.out.println("\ncodigo: "+getCodigo()+"precio:"+getPrecio()+"nombre:"+getNombre());
    
    }
    
    
}
