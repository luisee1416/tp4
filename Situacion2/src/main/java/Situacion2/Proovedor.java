package Situacion2;

public class Proovedor {


    private int codigoProovedor;
    private int razonSocial;
    private long telefono;
    private String email;

    public Proovedor(int codigoProovedor, int razonSocial, long telefono, String email) {
        this.codigoProovedor = codigoProovedor;
        this.razonSocial = razonSocial;
        this.telefono = telefono;
        this.email = email;
    }

    public int getCodigoProovedor() {
        return codigoProovedor;
    }

    public void setCodigoProovedor(int codigoProovedor) {
        this.codigoProovedor = codigoProovedor;
    }

    public int getRazonSocial() {
        return razonSocial;
    }

    public void setRazonSocial(int razonSocial) {
        this.razonSocial = razonSocial;
    }

    public long getTelefono() {
        return telefono;
    }

    public void setTelefono(long telefono) {
        this.telefono = telefono;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    
}
