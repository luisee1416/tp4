package Situacion2;

public abstract class Herramienta {

    private int codigo;
    private float precio;
    private String nombre;
    private String funcionalidad;

    public Herramienta(int codigo, float precio, String nombre, String funcionalidad) {
        this.codigo = codigo;
        this.precio = precio;
        this.nombre = nombre;
        this.funcionalidad = funcionalidad;
    }

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public float getPrecio() {
        return precio;
    }

    public void setPrecio(float precio) {
        this.precio = precio;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getFuncionalidad() {
        return funcionalidad;
    }

    public void setFuncionalidad(String funcionalidad) {
        this.funcionalidad = funcionalidad;
    }

    

    public  abstract void mostrarHerramienta();



    
}
