package Situacion1;

public class Cliente{


private String nombre;

private String apellido;

private String email;

private long telefono;

private long DNI;

private String asunto;

//private Domicilio domicilio[];

public Cliente(String nombre, String apellido,String email , long telefono,long DNI,String asunto){

    this.nombre=nombre;
    this.email=email;
    this.telefono=telefono;
    this.DNI=DNI;
    this.asunto=asunto;
    this.apellido=apellido;


}

public void setNombre(String nombre){

this.nombre=nombre;

}


public String getNombre(){

    return nombre;
}

public String getEmail() {
    return email;
}

public void setEmail(String email) {
    this.email = email;
}

public long getTelefono() {
    return telefono;
}

public void setTelefono(long telefono) {
    this.telefono = telefono;
}

public long getDNI() {
    return DNI;
}

public void setDNI(long DNI) {
    this.DNI = DNI;
}

public String getAsunto() {
    return asunto;
}

public void setAsunto(String asunto) {
    this.asunto = asunto;
}

public String getApellido() {
    return apellido;
}

public void setApellido(String apellido) {
    this.apellido = apellido;
}

@Override
public String toString() {
    return "Cliente [DNI=" + DNI + ", apellido=" + apellido + ", asunto=" + asunto + ", email=" + email + ", nombre="
            + nombre + ", telefono=" + telefono + "]";
}








}




