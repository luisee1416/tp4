package Situacion1;

public class DomicilioEdificio  extends Domicilio{

    private String departamento;
    private String calle;
    private int numeroCalle;
    private int piso;
    
  
    
    public String getDepartamento() {
        return departamento;
    }
    
    public void setDepartamento(String departamento) {
        this.departamento = departamento;
    }
    
    public int getPiso() {
        return piso;
    }
    
    public void setPiso(int piso) {
        this.piso = piso;
    }


	public String getCalle() {
		return calle;
	}

	public void setCalle(String calle) {
		this.calle = calle;
	}

	public int getNumeroCalle() {
		return numeroCalle;
	}

	public void setNumeroCalle(int numeroCalle) {
		this.numeroCalle = numeroCalle;
	}

	public DomicilioEdificio( String departamento, String calle, int numeroCalle, int piso) {
		
		this.departamento = departamento;
		this.calle = calle;
		this.numeroCalle = numeroCalle;
		this.piso = piso;
	}

	@Override
	public void mostrarDomicilio() {
		
		System.out.println("calle: "+getCalle() + "numero "+getNumeroCalle()+"piso "+getPiso());

	}





}



    

